## 用html编写笔记
灵活使用h1到h6做各级标题。内容用p标签，结合strong等标签，记录自己学习本门课程的心得与内容。

## markdown介绍
markdown文件，后缀名就是md，在github和码云上，项目里有md文件的话，直接打开可以看，会发现一般都是很漂亮的文档。markdown的语法很容易，经常用来写文档。也能方便的转换为html代码。

## 印象笔记
印象笔记就支持markdown写作。

## 总结
同学们可以自行学习markdown的语法，在自己项目的源代码中加入md文件，托管到码云后直接在浏览器打开就能看到非常漂亮的代码。

## java代码显示示例

```java
package Export;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

