## git commit的问题
提交的时候，如果输入：

```bash
git commit
```
会出现以下界面：

```
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
# Your branch is up to date with 'origin/master'.
#
# Changes to be committed:
#	new file:   "vim\351\200\200\345\207\272\346\223\215\344\275\234.md"
#
```
这里很多同学不知道怎么办了。这里实际上进入了vim编辑器，要输入提交日志后退出，怎么操作呢？

按键盘上的i  ,左下角出现

```
-- 插入 --
```
此时在光标所在处输入提交日志，输入完毕后，按ESC键若干下，然后输入：

```vim
:wq
```
再按回车即可退出。
如果不想如此麻烦，在提交的时候，请输入：

```git
git commit -m "提交日志"
```

